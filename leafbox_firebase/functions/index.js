const functions = require("firebase-functions");
const firebase = require("firebase-admin");
const express = require("express");
const path = require("path");
const cors = require('cors');
const constants = require('./const')
const whitelist = constants.WHITELIST_URL;
const products = require('./api/products');
const app = express();
const counter = require('./api/counter')

// app.use(cors({
//   origin: (origin, callback) => {
//     if (whitelist.indexOf(origin) !== -1) {
//       return callback(null, true)
//     } else {
//       return callback(new Error('Not allowed by CORS'))
//     }
//   }
// }));

app.get("/", (req, res) => {
  counter.incrementCounter('home');
  res.sendFile(path.join(__dirname, "/home/index.html")); //static app
});

app.use('/products', products)
app.use('/counters', counter.counterRouter)
exports.api = functions.https.onRequest(app);
