'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports.incrementCounter = exports.counterRouter = undefined;var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _express = require('express');var _express2 = _interopRequireDefault(_express);
var _firebaseAdmin = require('firebase-admin');var _firebaseAdmin2 = _interopRequireDefault(_firebaseAdmin);
var _moment = require('moment');var _moment2 = _interopRequireDefault(_moment);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}
const db = _firebaseAdmin2.default.firestore();
const counterRouter = exports.counterRouter = (0, _express.Router)();

const incrementCounter = exports.incrementCounter = (() => {var _ref = (0, _asyncToGenerator3.default)(function* (url) {
        const date = (0, _moment2.default)(new Date()).format('YYYYMMDD');
        const ref = db.collection(`${url}_traffic`).doc(date);
        ref.get().
        then(function (documentSnapshot) {
            const currentCount = documentSnapshot.exists ? documentSnapshot.data().count : 0;

            ref.set({
                count: Number(currentCount) + 1,
                date: (0, _moment2.default)().unix() },
            { merge: true }).
            then(function () {
                console.log(`${url} : Counter increased!`);
            });
        });
    });return function incrementCounter(_x) {return _ref.apply(this, arguments);};})();

counterRouter.get('/:id/:type', (() => {var _ref2 = (0, _asyncToGenerator3.default)(function* (req, res, next) {
        const counters = [];
        const dateRange = getDateRangeByType(req.params.type);

        const refSnapshot = yield db.collection(`${req.params.id}_traffic`).
        where("date", '>=', dateRange.from).
        where("date", '<=', dateRange.to).
        get();
        refSnapshot.forEach(function (doc) {
            counters.push(doc.data());
        });
        res.send(counters);
    });return function (_x2, _x3, _x4) {return _ref2.apply(this, arguments);};})());

const getDateRangeByType = type => {
    let dateRange = {
        from: null,
        to: null };

    if (type.toLowerCase() === 'month') {
        dateRange.from = (0, _moment2.default)(new Date()).startOf('month').unix();
        dateRange.to = (0, _moment2.default)(new Date()).endOf('month').unix();
    }
    if (type.toLowerCase() === 'week') {
        dateRange.from = (0, _moment2.default)(new Date()).startOf('week').unix();
        dateRange.to = (0, _moment2.default)(new Date()).endOf('week').unix();
    }
    if (type.toLowerCase() === 'year') {
        dateRange.from = (0, _moment2.default)(new Date()).startOf('year').unix();
        dateRange.to = (0, _moment2.default)(new Date()).endOf('year').unix();
    }
    return dateRange;
};