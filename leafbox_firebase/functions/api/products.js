'use strict';var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);var _express = require('express');var _express2 = _interopRequireDefault(_express);
var _firebaseAdmin = require('firebase-admin');var _firebaseAdmin2 = _interopRequireDefault(_firebaseAdmin);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

_firebaseAdmin2.default.initializeApp({
    credential: _firebaseAdmin2.default.credential.applicationDefault() });


const db = _firebaseAdmin2.default.firestore();

const products = (0, _express.Router)();
products.get('/', (() => {var _ref = (0, _asyncToGenerator3.default)(function* (req, res, next) {
        try {
            const productsSnapShot = yield db.collection('products').get();
            const products = [];
            productsSnapShot.forEach(function (doc) {
                products.push({
                    name: doc.name,
                    description: doc.description,
                    cost: doc.cost,
                    images: doc.images });

            });
            res.json(products);
        } catch (e) {
            next(e);
        }
    });return function (_x, _x2, _x3) {return _ref.apply(this, arguments);};})());

products.get('/:id', (() => {var _ref2 = (0, _asyncToGenerator3.default)(function* (req, res, next) {
        try {
            const id = req.params.id;
            if (!id) throw new Error('id is blank');
            const product = yield db.collection('products').doc(id).get();
            if (!product.exists) {
                throw new Error('product does not exists');
            }
            res.json({
                id: product.id,
                data: product.data() });

        } catch (e) {
            next(e);
        }
    });return function (_x4, _x5, _x6) {return _ref2.apply(this, arguments);};})());

products.post('/', (() => {var _ref3 = (0, _asyncToGenerator3.default)(function* (req, res, next) {
        try {
            const data = req.body.data;
            if (!data) throw new Error('Product is blank');
            const ref = yield db.collection('products').add(data);
            res.json({
                id: ref.id,
                data });

        } catch (e) {
            next(e);
        }
    });return function (_x7, _x8, _x9) {return _ref3.apply(this, arguments);};})());

products.put('/:id', (() => {var _ref4 = (0, _asyncToGenerator3.default)(function* (req, res, next) {
        try {
            const id = req.params.id;
            const data = req.body.data;
            if (!id) throw new Error('id is blank');
            if (!data) throw new Error('Product is blank');
            const ref = yield db.collection('products').doc(id).set(data, { merge: true });
            res.json({
                id,
                data });

        } catch (e) {
            next(e);
        }
    });return function (_x10, _x11, _x12) {return _ref4.apply(this, arguments);};})());

products.delete('/:id', (() => {var _ref5 = (0, _asyncToGenerator3.default)(function* (req, res, next) {
        try {
            const id = req.params.id;
            if (!id) throw new Error('id is blank');
            yield db.collection('products').doc(id).delete();
            res.json({
                id });

        } catch (e) {
            next(e);
        }
    });return function (_x13, _x14, _x15) {return _ref5.apply(this, arguments);};})());

module.exports = products;