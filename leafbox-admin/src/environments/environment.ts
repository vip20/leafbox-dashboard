// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD0-woTNmx544Pvk9jwmmryIz8EhINpLmE',
    authDomain: 'leafbox-kpv.firebaseapp.com',
    databaseURL: 'https://leafbox-kpv.firebaseio.com',
    projectId: 'leafbox-kpv',
    storageBucket: 'leafbox-kpv.appspot.com',
    messagingSenderId: '1004544698126'
  },
  API: 'http://localhost:5000/leafbox-kpv/us-central1/api'
};
