import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

const host = environment.API;

@Injectable()
export class DashboardService {

  constructor(private http: HttpClient) { }

  getHomeTrafficCount(type) {
    return this.http.get(`${host}/counters/home/${type}`);
  }
}
