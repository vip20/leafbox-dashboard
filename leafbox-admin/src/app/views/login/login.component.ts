import { Router } from '@angular/router';
import { AuthService } from './../../core/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {
  public msg: string;
  public msgType: string;
  public value = {
    email: '',
    password: ''
  };
  constructor(private authservice: AuthService,
    private router: Router) {}

  login() {
    this.authservice.doLogin(this.value).then(res => {
      this.msg = 'Login Success';
      this.msgType = 'alert-success';
      this.router.navigate(['/dashboard']);
    }, err => {
      console.log(err);
      this.msg = 'Invalid Credentials';
      this.msgType = 'alert-danger';
    });
  }
 }
